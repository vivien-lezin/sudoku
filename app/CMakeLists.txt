cmake_minimum_required(VERSION 3.28)
project(sudoku C)

set(CMAKE_C_STANDARD 11)

add_library(lib_sudoku SHARED ${CMAKE_CURRENT_SOURCE_DIR}/../lib/lib-sudoku/library.c
        ../lib/lib-sudoku/library.h)

add_executable(sudoku main.c
        ../lib/lib-sudoku/library.h)

target_link_libraries(sudoku PUBLIC lib_sudoku)