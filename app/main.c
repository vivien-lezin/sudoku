#include <stdio.h>

#include "../lib/lib-sudoku/library.h"

int isRowValid(int grid[SIZE][SIZE], const int row, const int num) {
    for (int i = 0; i < SIZE; i++) {
        if (grid[row][i] == num) {
            return 0;
        }
    }

    return 1;
}

int isColumnValid(int grid[SIZE][SIZE], const int col, const int num) {
    for (int i = 0; i < SIZE; i++) {
        if (grid[i][col] == num) {
            return 0;
        }
    }

    return 1;
}

int main() {
    int grid[SIZE][SIZE];
    generateSudoku(grid);

    int stop = 0;
    while (stop != 1) {
        printf("Grille de sudoku générée :\n");
        printGrid(grid);
        printf("-------------------------\n");
        printf("On arrête là ? (1) :");
        scanf("%d", &stop);
        printf("-------------------------\n");
        if (stop == 1) {
            continue;
        }
        int number=6;
        int col=1;
        int row=1;
        printf("Rentrez une valeur :");
        scanf("%d", &number);
        printf("Quelle colonne :");
        scanf("%d", &col);
        printf("Quelle ligne :");
        scanf("%d", &row);
        if (row < 1 || col < 1 || row > SIZE + 1 || col > SIZE + 1) {
            printf("/!\\ Hors grille\n");
            continue;
        }
        if (grid[row - 1][col - 1] != 0) {
            printf("/!\\ Il existe déjà une valeur dans cette case\n");
            continue;
        }

        if (isColumnValid(grid, col-1, number) == 0 || isRowValid(grid, row-1, number) == 0 ) {
            printf("Nope! La valeur n'est pas bonne\n");
        } else {
            grid[row - 1][col - 1] = number;
        }

        printf("##########################\n");
    }

    printf("Merci d'avoir joué :)\n");
    return 0;
}
