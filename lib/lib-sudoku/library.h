#ifndef LIB_SUDOKU_LIBRARY_H
#define LIB_SUDOKU_LIBRARY_H
#define SIZE 9
void printGrid(int grid[SIZE][SIZE]);

void generateSudoku(int grid[SIZE][SIZE]);

#endif //LIB_SUDOKU_LIBRARY_H
