#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SIZE 9

// Fonction pour afficher la grille de sudoku
void printGrid(int grid[SIZE][SIZE]) {
    printf("   ");
    for (int i = 1; i <= SIZE; i++) {
        printf("%d ", i);
    }
    printf("\n");
    printf("   ");;
    for (int i = 1; i <= SIZE; i++) {
        printf("| ");
    }
    printf("\n");
    for (int i = 0; i < SIZE; i++) {
        printf("%d--",i+1);
        for (int j = 0; j < SIZE; j++) {
            printf("%d ", grid[i][j]);
        }
        printf("\n");
    }
}

// Fonction pour vérifier si un nombre peut être placé dans une case
int isValid(int grid[SIZE][SIZE], int row, int col, int num) {
    // Vérifier la ligne
    for (int i = 0; i < SIZE; i++) {
        if (grid[row][i] == num) {
            return 0;
        }
    }

    // Vérifier la colonne
    for (int i = 0; i < SIZE; i++) {
        if (grid[i][col] == num) {
            return 0;
        }
    }

    // Vérifier la sous-grille 3x3
    int startRow = row - row % 3;
    int startCol = col - col % 3;
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            if (grid[i + startRow][j + startCol] == num) {
                return 0;
            }
        }
    }

    return 1;
}

// Fonction pour résoudre la grille de sudoku
int solveSudoku(int grid[SIZE][SIZE]) {
    int row, col;

    // Recherche d'une case vide
    int isEmpty = 0;
    for (row = 0; row < SIZE; row++) {
        for (col = 0; col < SIZE; col++) {
            if (grid[row][col] == 0) {
                isEmpty = 1;
                break;
            }
        }
        if (isEmpty == 1) {
            break;
        }
    }

    // Aucune case vide trouvée, la grille est résolue
    if (isEmpty == 0) {
        return 1;
    }

    // Essayer de placer les nombres 1 à 9 dans la case vide
    for (int num = 1; num <= 9; num++) {
        if (isValid(grid, row, col, num)) {
            grid[row][col] = num;

            // Résoudre récursivement la grille
            if (solveSudoku(grid)) {
                return 1;
            }

            // Si la solution n'est pas possible avec ce nombre, remettre la case à zéro
            grid[row][col] = 0;
        }
    }

    // Aucune solution possible
    return 0;
}

// Fonction pour générer une grille de sudoku valide
void generateSudoku(int grid[SIZE][SIZE]) {
    // Initialiser le générateur de nombres aléatoires
    srand(time(NULL));

    // Remplir la grille avec des zéros
    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            grid[i][j] = 0;
        }
    }

    // Résoudre la grille
    solveSudoku(grid);

    // Masquer certains nombres pour créer une grille de sudoku à résoudre
    int numToHide = (rand() % 30) + 20; // Nombre aléatoire de nombres à masquer (entre 20 et 50)
    for (int i = 0; i < numToHide; i++) {
        int row = rand() % SIZE;
        int col = rand() % SIZE;
        while (grid[row][col] == 0) {
            row = rand() % SIZE;
            col = rand() % SIZE;
        }
        grid[row][col] = 0;
    }
}

